package net.java.ao.schema;

public interface IndexNameConverter {
    String getName(String tableName, String fieldName);
}
